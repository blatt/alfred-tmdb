//'use strict';
require('dotenv').config({ path: __dirname + '/variables.env' })
const alfy = require('alfy');
// set environment variable in alfred settings!!!
const apiKey = process.env.TMDB_API_KEY;
const apiLanguage = process.env.LANGUAGE;
const apiRegion = process.env.REGION;

if (!apiKey) {
    alfy.error('First set the "TMD_API_KEY" variable in the workflow settings.');
    return;
}

alfy.fetch('api.themoviedb.org/3/search/multi', {
    query: {
        api_key: apiKey,
        language: apiLanguage,
        region: apiRegion,
        query: alfy.input,
    }
}).then(data => {
		const items = data.results

				.map((x) => {
					mediaType: x.media_type
            if (x.media_type === 'tv') {
                return {
                    title: x.name + ' (' + x.first_air_date.substr(0,4) + ')',
                    subtitle: '★ ' + x.vote_average + ' | OT: ' + x.original_name,
                    arg: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    autocomplete: x.name,
                    quicklookurl: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    icon: {
                        'path': 'icon_tv.png'
                    }
                };
            } else if (x.media_type === 'movie') {
                return {
                    title: x.title + ' (' + x.release_date.substr(0,4) + ')',
                    subtitle: '★ ' + x.vote_average + ' | OT: ' + x.original_title,
                    arg: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    autocomplete: x.title,
                    quicklookurl: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    icon: {
                        'path': 'icon_movie.png'
                    }
								};
							} else if (x.media_type === 'person') {
                return {
                    title: x.name,
                    arg: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    autocomplete: x.name,
                    quicklookurl: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    icon: {
                        'path': 'icon_person.png'
                    }
                };
            } else {
                return {
                    title: x.original_title,
                    arg: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                    autocomplete: x.name,
                    quicklookurl: 'https://themoviedb.org/' + x.media_type + '/' + x.id,
                }
						}
        });

    alfy.output(items);
});
