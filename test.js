import test from 'ava';
import alfyTest from 'alfy-test';

test(async t => {
	const alfy = alfyTest();
	const result = await alfy('doctor who: the end of time');

	t.deepEqual(result, [
		{
			arg: "https://themoviedb.org/movie/281982",
			autocomplete: "Doctor Who: The End of Time",
			icon:  {
				path: "icon_movie.png",
			},
			quicklookurl: "https://themoviedb.org/movie/281982",
			subtitle: "★ 8.4 | OT: Doctor Who: The End of Time",
			title: "Doctor Who: The End of Time (2009)",
		}
	]);
});
