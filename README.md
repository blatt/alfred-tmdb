## alfred-tmdb ![alfred-workflow](https://img.shields.io/badge/Alfred-workflow-5c1f87.svg)
> Search [themoviedb.org](https://www.themoviedb.org/) with Alfred 3.

This workflow uses the TMDb API but is not endorsed or certified by TMDb.

## Installation

```bash
npm install --global alfred-tmdb
```

*Requires [Node.js](https://nodejs.org) 4+, the Alfred [Powerpack](https://www.alfredapp.com/powerpack/) and an The Movie Database (TMDb) [API](https://www.themoviedb.org/documentation/api) key.*

## Config
Click the **[x]** in the top right of your workflow to show the Workflow Environment Variables panel.

Only the `TMDB_API_KEY` variable is required. If you don't have a Key, generate a free one [here](https://developers.themoviedb.org/3/getting-started/introduction).

NAME | VALUE
------------ | -------------
TMDB_API_KEY | your-api-key
LANGUAGE | en-US ([default](https://developers.themoviedb.org/3/getting-started/languages))
REGION | en ([default](https://developers.themoviedb.org/3/getting-started/regions))

## How to use

In Alfred, type `tmdb`, <kbd>Enter</kbd>, and your query.

## Screenshots

![screenshot](screenshot.png)
Screenshot is taken with german environment variables (de-DE) and the [eXon Theme](https://gitlab.com/blatt/alfred-themes#exon-theme)

## Build status

![pipeline status](https://gitlab.com/blatt/alfred-tmdb/badges/master/pipeline.svg) ![coverage report](https://gitlab.com/blatt/alfred-tmdb/badges/master/coverage.svg) ![npm](https://img.shields.io/npm/v/alfred-tmdb.svg) ![node](https://img.shields.io/node/v/alfred-tmdb.svg) ![downloads](https://img.shields.io/npm/dt/alfred-tmdb.svg)

## Code style

[![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo)

## Contribute

All contributions are welcome, if you have a feature request don't hesitate to open an issue!

## Development

TODO

## Tech/framework used

Built with

- [Alfy](https://github.com/sindresorhus/alfy)
- [generator-alfred](https://github.com/SamVerschueren/generator-alfred)
- [Yeoman](http://yeoman.io/)
- [GitLab Continuous Integration & Deployment | GitLab](https://about.gitlab.com/features/gitlab-ci-cd/)

## Credits

- Workflow icon/TMDb Logo: [The Movie Database](https://www.themoviedb.org)
- Other icons: [Font Awesome](http://fontawesome.io) by Dave Gandy
- Data source: TMDb [API Version 3](https://developers.themoviedb.org/3/)

## License

© Distributed under the terms of the [MIT License](https://gitlab.com/blatt/alfred-tmdb/raw/master/LICENSE).
